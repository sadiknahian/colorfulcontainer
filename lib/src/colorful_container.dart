import 'package:flutter/material.dart';

class ColorfulContainer extends StatefulWidget {
  const ColorfulContainer({
    Key? key,
    this.width,
    this.height,
    this.borderRadius,
    this.startingColor,
    this.endingColor,
    this.shadowColor,
    this.shadowBlurRadius,
    this.borderColor,
    this.borderWidth,
    this.containerPadding,
    this.shadowOffset,
    this.child = const Text('No child widget was passed'),
    this.callbackFunction,
  }) : super(key: key);

  final double? width;
  final double? height;
  final BorderRadius? borderRadius;
  final Color? startingColor;
  final Color? endingColor;
  final Color? shadowColor;
  final double? shadowBlurRadius;
  final Color? borderColor;
  final double? borderWidth;
  final EdgeInsets? containerPadding;
  final Offset? shadowOffset;
  final Widget? child;
  final VoidCallback? callbackFunction;

  @override
  State<ColorfulContainer> createState() => _ColorfulContainerState();
}

class _ColorfulContainerState extends State<ColorfulContainer> {
  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 1.5,
      borderRadius: widget.borderRadius ?? const BorderRadius.all(Radius.circular(10.0)),
      child: InkWell(
        onTap: () {
          if (widget.callbackFunction != null) widget.callbackFunction!();
        },
        child: Container(
          width: widget.width,
          height: widget.height,
          padding: widget.containerPadding ?? const EdgeInsets.fromLTRB(4, 8, 4, 8),
          decoration: BoxDecoration(
            borderRadius: widget.borderRadius ?? const BorderRadius.all(Radius.circular(10.0)),
            gradient: LinearGradient(
              colors: [widget.startingColor ?? Colors.tealAccent, widget.endingColor ?? Colors.teal],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            border: Border.all(
              color: widget.borderColor ?? const Color(0xFFF5F5F5),
              width: widget.borderWidth ?? 0.5,
            ),
            boxShadow: [
              BoxShadow(
                color: widget.shadowColor ?? Colors.grey,
                blurRadius: widget.shadowBlurRadius ?? 2,
                offset: widget.shadowOffset ?? const Offset(0, 0),
              ),
            ],
          ),
          child: Center(
            child: widget.child,
          ),
        ),
      ),
    );
  }
}
