## Features

This a simple package that can wrap a widget inside a container
...............................................................
    background : Editable (Gradiant Color)
    container radius: Editable (10) 
    border : Editable (Color, Width)
    boxShadow :  Editable(Color, BlurRadius, Offset)

## Getting started

prerequisites : sdk: '>=3.0.6 <4.0.0'
....................................
add `colorfulcontainerbysadiknahian: ^0.0.3` under "dependencies" in "pubspec.yaml"

## Usage

Use it as a widget, Give card type container to other widget


## Example
ColorfulContainer(
    child: Column(
        children: [
            Text('Test line 1'),
            Text('Test line 2'),
        ],
    ),
),

## Additional information
---By Sadik Nahian